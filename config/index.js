// const privateRoutes = require('./routes/privateRoutes');
// const publicRoutes = require('./routes/publicRoutes');
const adminRoutes = require('./routes/adminRoutes');
// const mockRoutes = require('./routes/mockRoutes');
// const cronRoutes = require('./routes/cronRoutes');

// require('dotenv').config();

const config = {
//   migrate: false,
//   privateRoutes,
//   publicRoutes,
  adminRoutes,
//   mockRoutes,
//   cronRoutes,
//   port: process.env.PORT || '2017',
};

module.exports = config;
