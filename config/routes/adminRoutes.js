// Routes : /v1/api/admin

const adminRoutes = {
    'POST /register': 'AdminController.register',
};
  
module.exports = adminRoutes;
  