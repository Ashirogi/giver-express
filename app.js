const express = require('express');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const path = require('path');
const app = express();
const cors = require('cors');
const http = require('http');
const mapRoutes = require('express-routes-mapper');

const port = 2000;

// create connection to database
// the mysql.createConnection function takes in a configuration object which contains host, user, password and the database name.
const db = mysql.createConnection ({
    host: 'localhost',
    user: 'root', 
    password: 'root',
    database: 'giver-dump'
});

// connect to database
db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
});
global.db = db;

// configure middleware
app.set('port', process.env.port || port); // set express to use this port
app.set('views', __dirname + '/views'); // set express to look in this folder to render our view
app.set('view engine', 'ejs'); // configure template engine
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // parse form data client
app.use(express.static(path.join(__dirname, 'public'))); // configure express to use public folder
app.use(fileUpload()); // configure fileupload

// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });

// configure to only allow requests from certain origins
app.use(cors());
const corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};
app.use(cors(corsOptions));

// routes for the app
const config = require('./config/');
const mappedAdminRoutes = mapRoutes(config.adminRoutes, 'api/controller/');

app.use('/v1/api/admin', mappedAdminRoutes);

const server = http.Server(app);

server.listen(port, () => {
    console.log('runing');
    // if (environment !== 'production' && environment !== 'development' && environment !== 'testing') {
    //   console.error(`NODE_ENV is set to ${environment}, but only production and development are valid.`);
    //   process.exit(1);
    // }
    return db;
});
  