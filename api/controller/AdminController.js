const notifMessage = require('../../libs/notifMassage');

const AdminController = () => {
  const register = async (req, res) => {
    const {userName, email, password } = req.body;

    // if (body.password === body.passwordConfirm) {
      // if (schema.validate(body.password)) {
        try {
          // const isAdminAlreadyExist = await Admin
          //   .findOne({
          //     where: {
          //       email: body.email,
          //     },
          //   });

          // if (isAdminAlreadyExist) {
          //   return res.status(400).json({
          //     msg: notifMessage.adminExist,
          //   });
          // }
          // const admin = await Admin.create({
          //   email: body.email.toLowerCase(),
          //   name: body.name,
          //   password: body.password,
          // });
          // const token = authService().issue({
          //   id: admin.id,
          // });

          // return res.status(200).json({
          //   token,
          //   admin,
          // });
          let stringQuery = `INSERT INTO user_giver( name, email, password, active, created_at, updated_at) 
          VALUES ( ?, ?, ?, 1, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP() );`; // query database to get all the players

          // execute query
          db.query(stringQuery, [userName, email, password], (err, result) => {
              if (err) {
                  console.log('got error :'+ err);
                  return res.status(500).json({
                      msg: notifMessage.internalServerError,
                  });
              }

              return res.status(200).json({
                  result: result,
              });
          });
        } catch (err) {
          console.log(err);
          return res.status(500).json({
            msg: notifMessage.internalServerError,
          });
        }
      // } else {
      //   return res.status(400).json({
      //     msg: notifMessage.errorRequiredMinimum,
      //   });
      // }
    // }

    // return res.status(400).json({
    //   msg: notifMessage.passwordNotMatch,
    // });
  };

  

  return {
    register,
  };
};

module.exports = AdminController;
